module.exports = function (app, addon) {

  var headless = require('../util/headless');
  var client = require('../util/client');

  // Render the mermaid macro.
  app.get('/v1/mermaid', addon.authenticate(), function(req, res){
    var pageId      = req.query['pageId'];
    if (pageId === undefined) {
      // Need to render as this is used for preview
      res.render('mermaid', {});
      return;
    }

    var pageVersion = req.query['pageVersion'],
        macroId     = req.query['macroId'],
        userKey     = req.query['userKey'];
    var clientKey = req.context.clientKey;
    var url = '/rest/api/content/' + pageId +
        '/history/' + pageVersion +
        '/macro/id/' + macroId;

    client.getHTTPClient(addon, clientKey, userKey).get(url,
      function (err, response, contents) {
        if (response && (response.statusCode == 404)) {
          // Need to render as this is used for preview
          console.log('Macro body 404', err, response);
          res.render('mermaid', {});
          return;
        }
        if (err || (response.statusCode < 200 || response.statusCode > 299)) {
          console.log('Macro body fetch failure', err, response);
          res.status(response.statusCode).send(null);
          return;
        } else {
          console.log('Macro body fetch success.', response, contents);
          var macro = JSON.parse(contents);
          if (macro.body.trim().startsWith("gantt")) {
            headless.render(macro.body, function(content) {
              var url = addon.config.localBaseUrl();
              if (!url.endsWith("/")) url += "/";
              url += `v1/fetch?id=${content}`;
              res.render('gantt', {url: url});
            });
          } else {
            res.render('mermaid', {body: macro.body});
          }
          return;
        }
     });
  });

};
