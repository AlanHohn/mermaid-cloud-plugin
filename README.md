Plugin for Atlassian Confluence cloud environments to support diagram rendering
with [Mermaid](https://github.com/knsv/mermaid).

Mermaid was primarily written by Knut Sveidqvist; see file THIRDPARTY for
license info.

## Using the Docker image

This repository includes a Dockerfile and docker-compose.yml to enable running
the plugin as a Docker container. There are a few defaults to configure and
some changes that will need to be made to properly set up the plugin as a Docker
container. However, unless code changes are needed, it should be possible to
make all of these changes in `docker-compose.yml`, so rebuilding the Docker
image should not be necessary.

As a result, using the Docker image comes down to these steps.
1. Copy `docker-compose.yml` to a new directory and update it as desired.
2. Create a `certs` directory and populate it with `server.crt` and `server.key`
   for the specific site.
3. Run `docker-compose up`.

However, this will use the settings for the standard "Mermaid Plugin for Confluence"
plugin as listed in the Marketplace. The following information assists in 
updating `docker-compose.yml` to publish it as a new plugin.

* Environment variables: The Dockerfile specifies environment variables used
  to configure the plugin. The defaults can be changed in the Dockerfile or
  overridden by supplying them in `docker-compose.yml`.

  * PLUGINKEY: The key to use for the plugin in atlassian-connect.json. This
    is used to uniquely identify the plugin in the Atlassian marketplace. The
    default is org.anvard.atlassian.mermaid-plugin.

  * LOCALBASEURL: The URL to the plugin. Used by Atlassian to find and invoke
    the plugin. This must use 'https' with a validly signed certificate and must 
    be accessible by the Atlassian Confluence Cloud servers. Defaults to
    "https://mermaid.anvard.org" in `docker-compose.yml`.

  * PORT: The port on which the plugin listens for connections. Defaults to 3000.
    Note that the Atlassian marketplace will only register plugins that can be
    accessed via the standard HTTP/S port of 443, so this default port of 3000
    assumes that some kind of port forwarding is taking place, either in
    `docker-compose.yml` or in a firewall.

  * DATABASE: The file path to the SQLite database that stores client connection
    information. Defaults to `/db/store.db`.

  * CERT: Location of the server certificate for SSL. Defaults to `/certs/server.crt`.

  * KEY: Location of the server private key for SSL. Defaults to `/certs/server.key`.

  * NODE_ENV: Choices include 'development' and 'production'. Defaults to production.

* Volumes: Volumes are used for persistent storage of information outside the container.

  * Database. The database should be in a volume outside the container so the container
    can be stopped and restarted without losing client information, as this would result
    in clients (Confluence cloud sites) having to uninstall and reinstall the plugin.
    By default, the database is a named volume created during the first `docker-compose up`
    command and is mounted on `/db`.

  * Certs. The server certificate and key are mounted in from the local file system so
    these secrets don't have to be in the image. By default they are found in a `certs`
    subdirectory under the location of the `docker-compose.yml` file and are mounted to
    `/certs`.

* Image name: By default this is alanhohn/mermaid-cloud-plugin but should be changed if
  an updated image is built and pushed.

The `scripts` directory contains scripts to help with building and running in Docker.
All scripts should be run from the base directory (e.g. `scripts/run-dev.sh`).

* `build.sh`: Runs `docker build` with the default tag name.

* `run-dev.sh`: Runs the Docker container with various development settings. This includes
  passing through `credentials.json`, which is used to connect a development instance of
  the plugin to a particular Confluence Cloud environment for testing. See the Atlassian
  Connect documentation for more information. 

* `run-dev-ssl.sh`: Like `run-dev.sh`, but also passes through a local `certs` directory
  to test launching the Docker container with SSL certs.

