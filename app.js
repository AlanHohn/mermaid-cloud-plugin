var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var compression = require('compression');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var morgan = require('morgan');

var ac = require('atlassian-connect-express');
ac.store.register('redis', require('atlassian-connect-express-redis'));

process.env.PWD = process.env.PWD || process.cwd(); // Fix expiry on Windows :(
var expiry = require('static-expiry');
var hbs = require('express-hbs');
var http = require('http');
var https = require('https');
var path = require('path');
var os = require('os');

var staticDir = path.join(__dirname, 'public');
var viewsDir = __dirname + '/views';
var routes = require('./routes');

var app = express();
var addon = ac(app);

var port = addon.config.port();
var devEnv = app.get('env') == 'development';

app.set('port', port);
app.engine('hbs', hbs.express3({partialsDir: viewsDir}));
app.set('view engine', 'hbs');
app.set('views', viewsDir);

app.use(morgan(devEnv ? 'dev' : 'combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(compression());
app.use(addon.middleware());
app.use(expiry(app, {dir: staticDir, debug: devEnv}));
hbs.registerHelper('furl', function(url){ return app.locals.furl(url); });
app.use(express.static(staticDir));

// Show nicer errors when in dev mode
if (devEnv) app.use(errorHandler());

routes(app, addon);

var server = null;
const CERT = process.env.CERT || './certs/server.crt';
const KEY = process.env.KEY || './certs/server.key';
if (fs.existsSync(CERT) && fs.existsSync(KEY)) {
  var options = {
    cert: fs.readFileSync(CERT),
    key: fs.readFileSync(KEY)
  };
  server = https.createServer(options, app);
} else {
  server = http.createServer(app);
}

server.listen(port, function(){
  console.log('Add-on server running at ' + os.hostname() + ':' + port);
  // Enables auto registration/de-registration of add-ons into a host in dev mode
  if (devEnv) addon.register();
});
