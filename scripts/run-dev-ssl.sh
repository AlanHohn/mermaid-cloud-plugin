#!/bin/bash
docker run --hostname localhost -it -p 3000:3000 -e "NODE_ENV=development" -e "http_proxy=" -e "https_proxy=" -v $(pwd)/credentials.json:/app/credentials.json -v $(pwd)/store.db:/app/store.db -v $(pwd)/certs:/certs alanhohn/mermaid-cloud-plugin
