#!/bin/bash
if [ -n "${PLUGINKEY}" ]
then
  sed -i "s/org.anvard.atlassian.mermaid-plugin/${PLUGINKEY}/" atlassian-connect.json
fi
npm start
