const child_process = require('child_process');

const ne = process.env.NODE_ENV;
if (ne !== undefined && ne == 'production') {
  console.log("Production environment, making sure needed libraries are present");
  child_process.execSync('apt-get update', {stdio:'inherit'});
  child_process.execSync('apt-get -y install libxss1 libgconf-2-4 libatk-bridge2.0-0 libgtk-3-0 libx11-xcb1 libnss3 libasound2', {stdio:'inherit'});
} else {
  console.log("Production not detected, nothing to do");
}
