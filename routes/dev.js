const puppeteer = require('puppeteer');
const headless = require('../util/headless');

const gantt = `
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
`;

module.exports = function (app, addon) {

  app.get('/dev/gantt', function(req, res){
    console.log("gantt");
    res.render('mermaid', {body: gantt});
  });
  
  app.get('/dev/export', function(req, res){
    console.log("export")
    headless.render('graph TB;A-->B', function(content){
      var url = addon.config.localBaseUrl();
      if (!url.endsWith("/")) url += "/";
      url += `v1/fetch?id=${content}`;
      res.render('export', {url: url});
    })
  })

  app.get('/dev/puppeteer', function(req, res){
    console.log("launching")
    puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']}).then(async browser => {
      console.log("creating page")
      const page = await browser.newPage()
      console.log("going to")
      const status = await page.goto('http://google.com')
      if (!status.ok) {
        throw new Error('cannot open google.com')
      }
      console.log("saving screenshot")
      const output = await page.screenshot()
      await browser.close()
      return output
    }).then((content) => {
      res.send(content);
    }).catch((e) => {
      console.log("Failed: " + e);
      res.status(500).send(null);
    })
  })

}
