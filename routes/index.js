module.exports = function (app, addon) {

  var devEnv = app.get('env') == 'development';

  app.get('/', function (req, res) {
    res.format({
      // TODO: Documentation HTML
      'text/html': function () {
        res.redirect('/atlassian-connect.json');
      },
      'application/json': function () {
        res.redirect('/atlassian-connect.json');
      }
    });
  });

  require("./macro")(app, addon);
  require("./export")(app, addon);

  if (devEnv) require("./dev")(app, addon);

};
