mermaid.mermaidAPI.initialize({
    startOnLoad:false
});
$(function(){
    var element = $("div.mermaid");

    var insertSvg = function(svgCode, bindFunctions){
        element.html(svgCode);
        AP.resize();
    };

    var graphDefinition = element.text();
    var graph = mermaid.mermaidAPI.render('mermaid', graphDefinition, insertSvg);
});
