const cache = require('memory-cache');
const fs = require('fs');
const path = require('path');
const uuidv4 = require('uuid/v4');

const createPuppeteerPool = require('puppeteer-pool');

const pool = createPuppeteerPool({
  max: 3,
  min: 1,
  puppeteerArgs: [{headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']}]
});

module.exports = {

  render: function(graph, cb) {
    pool.use(async (browser) => {
      const page = await browser.newPage()
      await page.goto('file://' + path.join(__dirname, 'export.html'), { waitUntil: 'networkidle' });
      await page.evaluate(`document.body.style.background = 'transparent'`);
      await page.evaluate((diag) => {
        window.mermaid.initialize({
          startOnLoad: true
        });
        window.mermaid.render('diag', diag, function(img) {
          $('#content').html(img);
        });
      }, graph);
      const clip = await page.$eval('svg', svg => {
        const r = svg.getBoundingClientRect();
        return { x: r.left, y: r.top, width: r.width, height: r.height };
      });
      const output = await page.screenshot({ clip, omitBackground: true });
      page.close();
      return output;
    }).then((output) => {
      const uuid = uuidv4();
      cache.put(uuid, output, 60000);
      cb(uuid);
    }).catch((e) => {
      console.log("Render failed: " + e);
      cb("Failed to render Mermaid diagram");
    });
  },

  fetch: function(uuid) {
    return cache.get(uuid);
  }
}
