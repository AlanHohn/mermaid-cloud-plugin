module.exports = {

  getHTTPClient: function(addon, clientKey, userKey) {
      return addon.httpClient({
          clientKey : clientKey,
          userKey   : userKey,
          appKey    : addon.key
      });
  }

}
