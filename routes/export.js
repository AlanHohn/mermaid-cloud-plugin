module.exports = function (app, addon) {

  var headless = require('../util/headless');
  var client = require('../util/client');

  app.get('/v1/fetch', function(req, res){
    var id = req.query['id'];
    if (id === undefined) {
      console.log("Bad request. No id");
      res.status(400).send(null);
      return;
    }
    res.send(headless.fetch(id));
  });

	// Render a mermaid diagram into a PNG.
	app.get('/v1/export', addon.authenticate(), function(req, res){
    var pageId      = req.query['pageId'];
    if (pageId === undefined) {
      console.log("Bad request. No pageId");
      res.status(400).send(null);
      return;
    }

    var pageVersion = req.query['pageVersion'],
        macroId     = req.query['macroId'],
        userKey     = req.query['userKey'];
    var clientKey = req.context.clientKey;
    var url = '/rest/api/content/' + pageId +
        '/history/' + pageVersion +
        '/macro/id/' + macroId;

    client.getHTTPClient(addon, clientKey, userKey).get(url,
      function (err, response, contents) {
        if (response.statusCode == 404) {
          console.log('Export failed due to 404 fetching macro body.', response);
          res.status(404).send(null);
          return;
        }
        if (err || (response.statusCode < 200 || response.statusCode > 299)) {
          console.log('Macro body fetch failure: ' + response);
          res.status(response.statusCode).send(null);
          return;
        } else {
          var macro = JSON.parse(contents);
          headless.render(macro.body, function(content) {
            var url = addon.config.localBaseUrl();
            if (!url.endsWith("/")) url += "/";
            url += `v1/fetch?id=${content}`;
            res.render('export', {url: url});
          });
          return;
        }
     });
	});
}
