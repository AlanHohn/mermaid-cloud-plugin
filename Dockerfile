FROM node:8

RUN apt-get update \
  && \
  apt-get -y install \
    libxss1 \
    libgconf-2-4 \
    libatk-bridge2.0-0 \
    libgtk-3-0 \
    libx11-xcb1 \
    libnss3 \
    libasound2 \
  && \
  apt-get clean \
  && \
  apt-get autoremove -y \
  && \
  rm -rf /var/lib/apt/lists/*

COPY . /app
WORKDIR /app

RUN npm install

ENV PORT=3000 DATABASE=/db/store.db CERT=/certs/server.crt KEY=/certs/server.key NODE_ENV=production
VOLUME /certs
VOLUME /db

CMD ["/bin/bash", "docker-entrypoint.sh"]

